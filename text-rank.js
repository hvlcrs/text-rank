function TextRank() {
  this.wordsMatrix = new Array();
  this.totalWords = 0;

  this.splitToParagraph = function (content) {
    return content.split('\n\n');
  }

  this.splitToSentence = function (content) {
    //return content.match(/[^\.!\?]+[\.!\?]+/g);
    return content.split('. ');
  }

  this.weighSentences = function (sentence) {
    var point = 0;
    var wordsPoint = new Array();
    var words = sentence.split(' ');

    for (w in words) {
      if (!wordsPoint[words[w]]) wordsPoint[words[w]] = 1;
      else wordsPoint[words[w]] += 1;
    }

    for (w in words) {
      if (typeof this.wordsMatrix[words[w]] != 'undefined') {
        //point += this.wordsMatrix[words[w]];
        point += this.calculateTfIdf(words[w], wordsPoint, words.length);
      }
    }

    return point;
  }

  this.calculateTfIdf = function(word, wordsPoint, totalWordsInSentence) {
    var point = 0;

    var tf = wordsPoint[word] / totalWordsInSentence;
    var idf = Math.log(this.totalWords / this.wordsMatrix[word]);

    return tf * idf;
  }

  this.getCentralSentence =  function (paragraph) {
    var sentences = this.splitToSentence(paragraph);
    var sentencePoint = new Array();

    if (!sentences)
      sentences = paragraph;

    for (s in sentences) {
      var point = this.weighSentences(sentences[s]);
      sentencePoint.push(point);
    }

    if (sentences.length > 1) {
      selected = 0;
      for (var i = 0; i < sentences.length; i++) {
        for (var j = i + 1; j < sentences.length; j++) {
          if (sentencePoint[i] >= sentencePoint[j]) {
            selected = i;
          }
        }
      }
    } else selected = 0;

    if(selected != null)
      return sentences[selected];
    else
      return '';
  }

  this.createWordMatrix = function (content) {
    var words = content.split(' ');
    this.totalWords = words.length;

    for (w in words) {
      if (!this.wordsMatrix[words[w]]) this.wordsMatrix[words[w]] = 1;
      else this.wordsMatrix[words[w]] += 1;
    }
  }

  this.createSummary = function (content) {
    var result = '';
    this.createWordMatrix(content);

    var paragraphs = this.splitToParagraph(content);
    for (p in paragraphs) {
      var centralSentence = this.getCentralSentence(paragraphs[p]);
      var resultSentence = centralSentence.replace('"', '');
      result += resultSentence;

      if (resultSentence[resultSentence.length - 1] != ".") result += '.';
      result += ' ';
      if (p % 3 == 0) result += '\n\n';
    }

    console.log('initial sentences: ' + this.splitToSentence(content).length);
    console.log('result sentences: ' + this.splitToSentence(result).length);

    return result;
  }
}
